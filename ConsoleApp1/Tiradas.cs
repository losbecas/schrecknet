﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacionDados
{
    class Tirada
    {
        private int reservaDados = 1;
        Random rnd = new Random();
        public int ReservaDados
        {
            get { return reservaDados; }
            set { reservaDados = value; }
        }
        public int TiraDados ()
        {
            int exitos = 0;
            for (int i = 0; i < reservaDados; ++i)
            {
                int resultado = rnd.Next(1, 11);
                if (resultado >= 6)                              //En "vampiro" los éxitos se consideran los dados con resultado igual o por encima de 6
                {
                    exitos++;
                }
            }
            return exitos;
        }
    }
}

   