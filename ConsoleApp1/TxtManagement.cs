﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FilesManagement
{
    class TxtManagement
    {
        private string file_path = "";
        public string File_path
        {
            get { return file_path; }
            set { file_path = value; }
        }
        public void Reader()
        {
            try
            {

                using (StreamReader sr = File.OpenText(file_path))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        Console.WriteLine(s);
                    }
                }
            }
            catch
            {
                Console.WriteLine("Invalid Document");
            }
        }
    }
}
