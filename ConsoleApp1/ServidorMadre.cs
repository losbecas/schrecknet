﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ServidorMadre
{
    class MotherServer
    {
        private string path_motherServer = @"C:\Users\Balma\Desktop\Proyectos\Curso C#\ConsoleApp1\Servidores\Servidor Madre"; // con esto permitimos que mas tarde se pueda navegar por las carpetas
        private string path_showed = @"C:";             //una es la mostrada, otra la que el programa usa para explorar los servidores
        private string ip = "182.190.0.55";
        public string Ip
        {
            get { return ip; }
            set { ip = value; }
        }
        public string Path_showed
        {
            get { return path_showed; }
            set { path_showed = value; }
        }
        public string Path_motherServer
        {
            get {return path_motherServer;}
            set { path_motherServer = value; }
        }
        public void ShowFiles()                 //Este método busca y muestra todos los archivos que se encuentran en un directorio
        {
            DirectoryInfo di = new DirectoryInfo(path_motherServer);
            foreach (var fi in di.GetFiles("*"))
            {
                Console.WriteLine(fi.Name);
            }
        }
        public void ShowDir()
        {
            DirectoryInfo di = new DirectoryInfo(path_motherServer);
            foreach (var fi in di.GetDirectories("*"))
            {
                Console.WriteLine(fi.Name);
            }
           
        }
        public string CompareDir(string folder)
        {
            DirectoryInfo di = new DirectoryInfo(path_motherServer);
            int x = 0;
            foreach(var fi in di.GetDirectories("*"))
            {
                x=string.Compare(folder, "cs "+ fi.Name); 
                if (x==0)               // si x = 0 significa que el nombre del directorio esta en la misma posición en comando y en directorio es decir, se ha introducido por ejemplo "cs Files"
                {
                    return fi.Name;         // En el ejemplo anterior aquí devolvería solo "Files" lo cual es extremadamente útil
                }
            }
            return "0";
        }
        public string CompareFile(string File)
        {
            DirectoryInfo di = new DirectoryInfo(path_motherServer);
            int x = 0;
            foreach(var fi in di.GetFiles("*"))
            {
                x = string.Compare(File, "open " + fi.Name);
                if(x==0)
                {
                    return fi.Name;
                }
            }
            return "0";
        }
        public void ShowWhereIam()
        {
            Console.WriteLine(path_motherServer);
            Console.WriteLine(path_showed);
        }
        public void GetIntoPath(string csFolder)
        {
            path_motherServer += "\\"+ csFolder;
            path_showed += "\\" + csFolder;
        }
        public void GetOutofPath(string cdFolder)
        {
            bool x = path_motherServer.EndsWith(cdFolder);          // comprobamos que el final de la dirección de directorio coincide con la palabra guardada a borrar
            if (x)
            {
                path_motherServer = path_motherServer.Remove(path_motherServer.Length - (cdFolder.Length + 1));     //el +1 es para eliminar la barra oblicua final
                path_showed = path_showed.Remove(path_motherServer.Length - (cdFolder.Length + 1));
            }
            else
                Console.WriteLine("Error: You can't get out of this directory");
            
        }
    }
}
    
