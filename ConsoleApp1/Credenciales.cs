﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AplicacionDados;
using System.IO;

namespace Credentials
{
    class BuscaUserPass         // Este programa busca en Credentials.txt coincidencia con usuario y contraseña
    {
        private string login_name;
        private string login_pass;
        private int user_id;
        public string Login_name
        {
            set { login_name = value;}
            get { return login_name; }
        }
        public string Login_pass
        {
            set { login_pass = value; }
        }
        public int User_id
        {
            get { return user_id; }
        }
        public bool Corroboracion()
        {
            string path_Credentials = @"C:\Users\Balma\Desktop\Proyectos\Curso C#\ConsoleApp1\Credentials.txt";      //esta es la ruta donde se encuentra el archivo txt, PUEDE VARIAR DEPENDIENDO DEL ORDENADOR
            string[] credentials = File.ReadAllLines(path_Credentials);
            int i = 0;
            bool access = false;
            string user = login_name+"="+login_pass;
            while (i < credentials.Length)
            {
                if (credentials[i] == user)
                {
                    access = true;
                    Console.WriteLine("Access Granted...");
                    break;                  //Si se llega aquí, significa que ha habido una coincidencia, no es necesario seguir buscando
                }
                i++;
            }
            user_id = i;
            return access;
        }
    }

    class ReadSheet       //Este objeto se encarga de cargar la ficha de personaje dependiendo del usuario que use la aplicacion
    {
        private int user_id;
        private string user_name;
        private string path_sheet;
        public int User_id
        {
            set { user_id = value; }
        }
        public string User_name
        {
            set { user_name = value; }
        }
        public int GetPool()
        {
            int user_pool = 0;
            path_sheet = @"C:\Users\Balma\Desktop\Proyectos\Curso C#\ConsoleApp1\Personajes\" + user_name + ".txt";         //con el nombre de usuario descirnimos su path
            string[] content_sheet = File.ReadAllLines(path_sheet);             //Con esto podemos seleccionar que reserva se usará en que momento ACTUALIZAR EN SIGUIENTES VERSIONES
            user_pool = Int32.Parse(content_sheet[0]);
            return user_pool;
        }
    }
}