﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AplicacionDados;
using Credentials;
using System.IO;
using ServidorMadre;
using FilesManagement;


namespace Schrecknet
{
    class Program
    {
        static int Main(string[] args)
        {
            Console.Title = "Schrecknet";
            int reserva=0;
            Tirada T = new Tirada();
            Console.Write("User: ");
            BuscaUserPass U = new BuscaUserPass();
            U.Login_name = Convert.ToString(Console.ReadLine());
            Console.Write("Pass: ");
            U.Login_pass = Convert.ToString(Console.ReadLine());
            Console.Clear();
            bool ack_user=U.Corroboracion();
            if (ack_user == true)
            {
                    Console.WriteLine("Welcome to Schrecknet.");
            }
            else
            {
                Console.WriteLine("You are not Welcome Here.");
                return 0;
            }
            Console.WriteLine(".");
            Console.Beep();
            Console.WriteLine(".");
            Console.Beep();
            Console.WriteLine(".");
            Console.Beep();
            ReadSheet PC = new ReadSheet();
            PC.User_name = U.Login_name;
            reserva= PC.GetPool();
            T.ReservaDados = reserva;
            int resultado;
            resultado = T.TiraDados();
            MotherServer mServer = new MotherServer();
            TxtManagement TextFile = new TxtManagement();

            string command = " ";
            string folder = "";
            string file = "";
            while (command != "exit")
            {
                mServer.ShowWhereIam();
                command = Console.ReadLine();
                Console.Beep();
                switch (command)
                {
                    case "exit":
                        break;
                    case "ls":
                        mServer.ShowDir();
                        mServer.ShowFiles();
                        break;
                    case "clear":
                        Console.Clear();
                        break;
                    case "cd":
                        mServer.GetOutofPath(folder);
                        break;
                     default:
                        string[] commands = command.Split(' ');
                        switch (commands[0])
                        {
                            case "cs":                               
                                folder = mServer.CompareDir(command);            // De momento solo comprueba directorios del ServidorMadre    // Devuelve el nombre del directorio introducido 
                                if (folder != "0")                      // Se comprueba que el directorio existe  // te dice si esta a la izquierda o a la derecha, no que no exista
                                {
                                    mServer.GetIntoPath(folder);
                                }
                                break;
                            case "open":
                                file = mServer.CompareFile(command);
                                Console.WriteLine(file);
                                if (file != "0")
                                {
                                    TextFile.File_path = mServer.Path_motherServer + "\\" + file;
                                    Console.WriteLine();
                                    TextFile.Reader();
                                    Console.WriteLine();
                                }
                                break;
                        }
                        break;
                }
            }
            return 0;
        }
    }
}
